let expect = chai.expect;

describe('Test de reservarHorario()', function(){
	it('Apartando un horario del restaurant (el horario ya no estará en el arreglo)', function(){
		let restaurant = new Restaurant(1, 'Mostaza', 'Hamburguesas', 'CABA', ['13:00', '17:00', '19:30'], 'logojpg', [4, 2, 8, 6]);
        restaurant.reservarHorario('17:00');
        //console.table(restaurant.horarios);
        expect(restaurant.horarios).to.be.an('array').that.does.not.include("17:00");
    })	

    it('Apartando un horario que no posee el restaurant', function(){
        let restaurant = new Restaurant(1, 'Mostaza', 'Hamburguesas', 'CABA', ['13:00', '17:00', '19:30'], 'logojpg', [4, 2, 8, 6]);
        let horariosDisponibles = restaurant.horarios.length;
        //console.log(horariosDisponibles);
        restaurant.reservarHorario('9:00', '19:00');       
        expect(horariosDisponibles).to.equal(3);
    })	

    it('Invocando a la función sin pasar parámetros', function(){
        let restaurant = new Restaurant(1, 'Mostaza', 'Hamburguesas', 'CABA', ['13:00', '17:00', '19:30'], 'logojpg', [4, 2, 8, 6]);
        let horariosDisponibles = restaurant.horarios.length;
        restaurant.reservarHorario();
        expect(horariosDisponibles).to.equal(3);
    })

    it('Corroborando que el arreglo se redujo en uno al reservar una hora disponible', function(){
        let restaurant = new Restaurant(1, 'Mostaza', 'Hamburguesas', 'CABA', ['13:00', '17:00', '19:30'], 'logojpg', [4, 2, 8, 6]);
        let horariosDisponibles = restaurant.horarios.length;
        restaurant.reservarHorario("13:00");
        //console.log(restaurant.horarios.length);
        expect(restaurant.horarios.length).to.equal(horariosDisponibles - 1);        
    })
});

describe('Test de obtenerPuntucación()', function(){
    it('Corroborando el tipo de dato que retorna es un numero o un float', function(){
        let restaurant = new Restaurant(1, 'Mostaza', 'Hamburguesas', 'CABA', ['13:00', '17:00', '19:30'], 'logojpg', [4, 2, 8, 6, 9]);
        //console.log(restaurant.obtenerPuntuacion());
        expect(restaurant.obtenerPuntuacion()).to.be.a('number' || 'float')
    })

    it('Calculando el promedio de puntuación correctamente (suponiendo que efectivamente el restaurant ha sido puntuado)', function(){
        let restaurant = new Restaurant(1, 'Mostaza', 'Hamburguesas', 'CABA', ['13:00', '17:00', '19:30'], 'logojpg', [4, 2, 8, 6, 9]);
        //console.log(restaurant.obtenerPuntuacion());
        expect(restaurant.obtenerPuntuacion()).to.equal(5.8);
    })
    
    it('Si no tiene puntuaciones el valor del promedio debe ser igual a 0', function(){
        let restaurant = new Restaurant(1, 'Mostaza', 'Hamburguesas', 'CABA', ['13:00', '17:00', '19:30'], 'logojpg', []);
        //console.log(restaurant.obtenerPuntuacion());
        expect(restaurant.obtenerPuntuacion()).to.equal(0);
    })    
});

describe('Test de calificar()', function(){
    it('pusheando nuevaCalificacion al arreglo fe calificaciones', function(){
		let restaurant = new Restaurant(1, 'Mostaza', 'Hamburguesas', 'CABA', ['13:00', '17:00', '19:30'], 'logojpg', [4, 2, 8, 6]);
        let cantidadCalificaciones = restaurant.calificaciones.length;
        //console.log(cantidadCalificaciones);
        restaurant.calificar(6);
        expect(restaurant.calificaciones.length).to.equal(cantidadCalificaciones + 1);
    })

    it('invocando a la función calificar sin pasar parametros', function(){
		let restaurant = new Restaurant(1, 'Mostaza', 'Hamburguesas', 'CABA', ['13:00', '17:00', '19:30'], 'logojpg', [4, 2, 8, 6]);
        let cantidadCalificaciones = restaurant.calificaciones.length;
        restaurant.calificar();
        //console.log(cantidadCalificaciones);
        expect(restaurant.calificaciones.length).to.equal(4);
    })
});

describe('test buscarRestaurante()',function(){
    let arregloRestaurantes = [
        new Restaurant(1, 'Mostaza', 'Hamburguesas', 'CABA', ['13:00', '17:00', '19:30'], 'logojpg', [4, 2, 8, 6]),
		new Restaurant(2, 'Burger king', 'Hamburguesas', 'CABA', ['14:00', '18:00', '17:30'], 'logojpg', [3, 6, 7, 8, 2]),
		new Restaurant(3, 'Kentucky', 'pizzas', 'Cordoba', ['11:00', '12:00', '14:00'], 'logojpg', [5,6,8,3,9,5])
    ];
    let testListado = new Listado(arregloRestaurantes);
    
    it('testeando buscarRestaurante() al pasar un id existente', function(){
        let restaurante = testListado.buscarRestaurante(2);
        expect(restaurante.id).to.equal(2);
    })

    it('si se pasa un id distinto muestra el error del else', function(){
        let restaurante = testListado.buscarRestaurante(4);
        expect(restaurante).to.have.string("No se ha encontrado ningún restaurant");
    })
});

describe('Test obtenerRestaurantes', function(){
    let arregloRestaurantes = [
        new Restaurant(1, 'Mostaza', 'Hamburguesas', 'CABA', ['11:00','13:00', '17:00', '19:30'], 'logojpg', [4, 2, 8, 6]),
		new Restaurant(2, 'Burger king', 'Hamburguesas', 'CABA', ['14:00', '18:00', '17:30'], 'logojpg', [3, 6, 7, 8, 2]),
		new Restaurant(3, 'Kentucky', 'pizzas', 'Cordoba', ['11:00', '12:00', '14:00'], 'logojpg', [5,6,8,3,9,5])
    ];
    let testListado = new Listado(arregloRestaurantes);

    it('Pasando solo el parámetro filtroRubro y los otros dos vacíos', function(){
        let filtro =  testListado.obtenerRestaurantes('pizzas', null, null)
        //console.log(filtro)
        expect(filtro.length).to.equal(1);
    })

    it('pasando solo el segundo filtro: filtroCiudad y los otros vacíos', function(){
        let filtro =  testListado.obtenerRestaurantes(null, 'CABA' , null)
        expect(filtro.length).to.equal(2);
    })

    it('pasando solo el tercer parámetro: filtroHorario', function(){
        let filtro = testListado.obtenerRestaurantes(null, null, '11:00')
        expect(filtro).to.be.an('array')
        expect(filtro.length).to.equal(2);
    })

    it('todos los parámetros vacíos/null', function(){
        let filtro = testListado.obtenerRestaurantes(null, null, null)
        //console.log(filtro)
        expect(filtro).to.be.an('array')
        expect(filtro.length).to.equal(3);
    })

});

describe('Test reserva.js', function(){    
    let reserva1 = new Reserva (new Date(2018, 7, 24, 11, 00), 8, 350, "DES1")
    let reserva2 = new Reserva (new Date(2018, 7, 27, 14, 100), 2, 150, "DES200")    

    it('Calcula el precio base de la reserva multiplicando precioPorPersona * cantidadPersonas', function(){
        //console.log(reserva1.cantidadPersonas);
        expect(reserva1.precioBase()).to.equal(2800)
    })

    it('tieneAdicional()', function(){
       expect(reserva1.tieneAdicional()).to.equal(true)
    })

    it('porcentajeAdicionalPorHorario()', function(){
       expect(reserva1.porcentajeAdicionalPorHorario()).to.equal(0)
    })

    it('porcentajeAdicionalPorFinde()', function(){
       expect(reserva1.porcentajeAdicionalPorFinde()).to.equal(280)
    })

    it('tieneDescuento()', function(){
       expect(reserva1.tieneDescuento()).to.equal(true)
    })

    it('porcentajeDescuentoPorGrupos()', function(){
       expect(reserva1.porcentajeDescuentoPorGrupos()).to.equal(280)
    })

    it('porcentajeDescuentoPorCodigo()', function(){
       expect(reserva1.porcentajeDescuentoPorCodigo()).to.equal(350)
    })    

    it('Calcula el pricioFinal() de la reserva restando descuentos y sumando adicionales ', function(){
       expect(reserva2.precioFinal()).to.equal(100)
    })
})
// expect(“casa”).to.be.a('string'); //se espera que “casa” sea un string

// expect([]).to.be.an(‘array’); //se espera que [] sea un arreglo

// expect(miValor).to.equal(3); //equal compara que miValor sea igual a 3

// expect({a: 1}).to.eql({a: 1}).but.not.equal({a: 1}); //eql compara cada valor del objeto. Para objetos usamos eql en vez de equal.
