var Reserva = function(horario, cantidadPersonas, precioPorPersona, codigoDescuento) {
    this.horario = horario;
    this.cantidadPersonas = cantidadPersonas;
    this.precioPorPersona = precioPorPersona;
    this.codigoDescuento = codigoDescuento;  
}

Reserva.prototype.precioBase = function(){
    return this.precioPorPersona  * this.cantidadPersonas;
}

Reserva.prototype.precioFinal = function(){
    if(this.tieneAdicional() == true || this.tieneDescuento() == true){
        console.log("precio final tiene modificación")
        return this.precioBase() + this.porcentajeAdicionalPorHorario() + this.porcentajeAdicionalPorFinde()
        - this.porcentajeDescuentoPorGrupos() - this.porcentajeDescuentoPorCodigo()
    }else {
        console.log("precio final no tiene modificaciones")
        return this.precioBase()
    }
}

Reserva.prototype.porcentajeAdicionalPorHorario = function(){
    //console.log(this.horario)
    if(this.horario.getHours() === 13 || this.horario.getHours() === 14 || this.horario.getHours() === 20 || this.horario.getHours() === 21){
        console.log("con adicional por horario")
        return this.precioBase() * 0.05
    }else{
        console.log("sin adicional horario")
        return 0
    }
}

Reserva.prototype.porcentajeAdicionalPorFinde = function(){
    if(this.horario.getDay() === 5 || this.horario.getDay() === 6 || this.horario.getDay() === 0){
        console.log("con adicional por finde")
        console.log(this.precioBase() * 0.10)
        return this.precioBase() * 0.10
    }else{
        console.log("sin adicional por finde")
        return 0
    }
}

Reserva.prototype.tieneAdicional = function(){
    if(this.horario.getHours() === 13 || this.horario.getHours() === 14 || this.horario.getHours() === 20 ||
        this.horario.getHours() === 21 || this.horario.getDay() === 5 || this.horario.getDay() === 6 || 
        this.horario.getDay() === 0){
            console.log("tiene adicional")
            return true
        }else{
            console.log("no tiene adicional")
            return false
        }
}

Reserva.prototype.porcentajeDescuentoPorGrupos = function(){
    if(this.cantidadPersonas >= 4 && this.cantidadPersonas <= 6){
        console.log("descuento por grupos 1")
        return this.precioBase() * 0.05
    }else if(this.cantidadPersonas >= 6 && this.cantidadPersonas <= 8){
        console.log("descuento por grupos 2")
        console.log(this.precioBase() * 0.10)
        return this.precioBase() * 0.10
    }else if(this.cantidadPersonas > 8){
        console.log("descuento por grupos 3")
        return this.precioBase() * 0.15
    }else{
        console.log("sin descuento por grupos")
        return 0
    }
}

Reserva.prototype.porcentajeDescuentoPorCodigo = function(){
    if(this.codigoDescuento == 'DES15'){
        console.log("descuento por codigo 15%")
        return this.precioBase() * 0.15        
    }else if(this.codigoDescuento == 'DES200'){
        console.log("descuento por codigo 200$")
        return 200        
    } else if(this.codigoDescuento == 'DES1'){
        console.log("descuento por codigo por una persona")
        console.log(this.precioPorPersona)
        return this.precioPorPersona
    } else {
        console.log("sin descuento por codigo")
        return 0
    }
}

Reserva.prototype.tieneDescuento = function(){
    if(this.cantidadPersonas >= 4 && this.cantidadPersonas <= 6 || this.cantidadPersonas >= 6 && this.cantidadPersonas <= 8||
        this.cantidadPersonas > 8 || this.codigoDescuento == 'DES15' || this.codigoDescuento == 'DES200'||this.codigoDescuento == 'DES1') {
            console.log("tiene descuento")
            return true
        }else{
            console.log("no tiene descuento")
            return false
        }
}

let reserva1 = new Reserva (new Date(2018, 7, 24, 11, 00), 8, 350, "DES1")
let reserva2 = new Reserva (new Date(2018, 7, 27, 14, 100), 2, 150, "DES200")
let reserva3 = new Reserva (new Date(2019, 6, 19, 16, 00), 4, 150, "DES15")
let reserva4 = new Reserva (new Date(2019, 6, 16, 20, 00), 2, 150,)