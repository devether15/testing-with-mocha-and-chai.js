let expect = chai.expect; 

describe('La inicialización de la lista de invitados', function(){
	it('debe tener cantidad 0',function(){
		expect(Invitados.cantidad).to.equal(0);
	})
	it('debe comenzar vacía',function(){
		expect(Invitados.nombres).eql([]);
	})
})

describe('Agregar un invitado', function(){
it('debe agregar un elemento al arreglo',function(){
	let cantidadAntesDeModificacion = Invitados.cantidad;
	Invitados.agregarInvitado('Juan');
	expect(Invitados.cantidad).to.equal(cantidadAntesDeModificacion+1);
	expect(Invitados.nombres).eql(['Juan']);
})})

describe('Eliminar un invitado', function(){
it('debe eliminar un elemento al arreglo',function(){
	let cantidadAntesDeModificacion = Invitados.cantidad;
	Invitados.eliminarUltimoInvitado();
	expect(Invitados.cantidad).to.equal(cantidadAntesDeModificacion-1);
	expect(Invitados.nombres).eql([]);
})})
