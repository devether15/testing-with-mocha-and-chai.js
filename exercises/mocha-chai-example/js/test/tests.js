let expect = chai.expect;

describe('Test de suma', function(){
	it('resultado positivo', function(){
		let a = suma(2,3);
		expect(a).to.be.above(0);
	})
	it('resultado negativo', function(){
		let a = suma(-1,-3);
		expect(0).to.be.above(a);
	})
});
