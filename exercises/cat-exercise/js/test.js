//testing the Cat instance
function testCatPropertiesOk(){
	var myCat = new Cat();
	var catName = myCat.name;
	var catGender = myCat.gender;

	return catName === 'Garfield' && catGender === 'male';
}

//test to see if more properties than expected are created
function testQuantityOfDefaultProperties(){
	var myCat = new Cat();
	var propertiesOfCat = Object.keys(myCat);
	return propertiesOfCat.length == 2;
}

//test to see if setName() configures the name correctly
function testSettingName(){
	var myCat = new Cat();
	myCat.setName('Pedro');
	return myCat.name === 'Pedro' && myCat.gender === 'male';
}

//test to see if setGender() configures the gender correctly
function testSetGender(){
	var myCat = new Cat();
	myCat.setGender('female');
	return myCat.gender === 'female' && myCat.name === 'Garfield';
}

//test to see if setGender() does not set the gender if it is invalid
function testIntroducingWrongGender(){
	var myCat = new Cat();
	myCat.setGender('Donald Trump');
	return myCat.gender === 'male';
}

//test to see if testSetGenderFemale configures the gender correctly when it's female
function testSetGenderFemale(){
	var myCat = new Cat();
	myCat.setGenderFemale();
	return myCat.gender === 'female' && myCat.name === 'Garfield';
}

function correrTests(){
	document.getElementById('testPropC').innerHTML = 'testCatPropertiesOk - Test Result: ' + '<b>'+testCatPropertiesOk()+'</b>';
	document.getElementById('testCantP').innerHTML = 'testQuantityOfDefaultProperties - Test Result: ' + '<b>'+testQuantityOfDefaultProperties()+'</b>';
	document.getElementById('testConfGC').innerHTML = 'testSetGender - Test Result: ' + '<b>'+testSetGender()+'</b>';
	document.getElementById('testConfN').innerHTML = 'testSettingName - Test Result: ' + '<b>'+testSettingName()+'</b>';
	document.getElementById('testConfGI').innerHTML = 'testIntroducingWrongGender - Test Result: ' + '<b>'+testIntroducingWrongGender()+'</b>';
	document.getElementById('testConfGH').innerHTML = 'testSetGenderFemale - Test Result: ' + '<b>'+testSetGenderFemale()+'</b>';
}