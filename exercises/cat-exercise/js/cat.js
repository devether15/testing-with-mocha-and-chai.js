// define the class
let Cat = function() {
  this.name = 'Garfield';
  this.gender = 'male';
};

//setting name
Cat.prototype.setName = function(name) {
  this.name = name;
};

//setting gender
Cat.prototype.setGender = function(gender) {
	if(gender === 'female' || gender === 'macho'){
		this.gender = gender;
	}
};

//setting gender when it's female
Cat.prototype.setGenderFemale = function() {
  this.setGender('female');
};
